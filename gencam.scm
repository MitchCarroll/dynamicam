#!/usr/bin/guile
;!#

(define hex (lambda (n) ;only works from 0 to 15
	      (let ((digits
		     '(#\0 #\1 #\2 #\3
		       #\4 #\5 #\6 #\7
		       #\8 #\9 #\a #\b
		       #\c #\d #\e #\f)))
		(list-ref digits n))))

(define hex-hi (lambda (n)
	  (quotient n 16)))

(define hex-lo (lambda (n)
	  (remainder n 16)))
       
(define hex-color (lambda (r g b)
		    (list->string (map hex `(,(hex-hi r) ,(hex-lo r)
				       ,(hex-hi g) ,(hex-lo g)
				       ,(hex-hi b) ,(hex-lo b))))))

(define list->color (lambda (n)
		      (hex-color (car n) (cadr n) (caddr n))))

(define random-color (lambda ()
		       (hex-color (random 256) (random 256) (random 256))))

(define make-random-colors (lambda (n)
	       (if (<= n 0)
		   '()
		   (cons (random-color) (make-random-colors (- n 1))))))			   

(define make-layer-sizes (lambda (n)
			   (if (<= n 1)
			       `()
			       (cons n (make-layer-sizes (quotient n 2))))))

(define random-camo (lambda (num-colors size file)
		      (let ((layers (make-layer-sizes size))
			    (colors (make-random-colors num-colors))
			    (outfile (open-output-file file)))
			
			(display size outfile) (display #\ outfile) (display size outfile) (newline outfile)
			(display (length layers) outfile) (display #\ outfile) (display (length colors) outfile) (newline outfile)
			(map (lambda (n)
			       (display n outfile) (display #\ outfile) (display n outfile) (newline outfile))
			     layers)
			(map (lambda (n)
			       (display n outfile) (display #\ outfile) (display n outfile) (newline outfile))
			     colors)
			(close-output-port outfile))))
			   
			   
(define make-camo (lambda (color-list size file) ;color-list is a list of triples of integers between 0 and 255
		      (let ((layers (make-layer-sizes size))
			    (colors (map list->color color-list))
			    (outfile (open-output-file file)))
			(display size outfile) (display #\ outfile) (display size outfile) (newline outfile)
			(display (length layers) outfile) (display #\ outfile) (display (length colors) outfile) (newline outfile)
			(map (lambda (n)
			       (display n outfile) (display #\ outfile) (display n outfile) (newline outfile))
			     layers)
			(map (lambda (n)
			       (display n outfile) (newline outfile))
			     colors)
			(close-output-port outfile))))

(define get-color (lambda ()
		    (let ((red 0)
			  (green 0)
			  (blue 0))
		      (display "enter the components of the color (0-255 only)") (newline)
		      (display "Red: ")
		      (set! red (read))
		      (display "Green: ")
		      (set! green (read))
		      (display "Blue: ")
		      (set! blue (read))
		      (list `(,red ,green ,blue)))))

(define get-all-colors (lambda (colors)
			 (let ((choice #\ ))
			   (display "do you want to enter another color? (y/n)")
			   (set! choice (read))
			   (case choice
			     ((n) colors)
			     ((N) colors)
			     ((y) (set! colors (append colors (get-color)))
			      (get-all-colors colors))
			     ((Y) (set! colors (append colors (get-color)))
			      (get-all-colors colors))))))
(define do-random-camo (lambda ()
			 (let ((num-col 0)
			       (size 0)
			       (file ""))
			   (display "enter filename: ")
			   (set! file (symbol->string (read)))
			   (display "enter number of colors: ")
			   (set! num-col (read))
			   (display "enter the size (wide or high) in pixels: ")
			   (set! size (read))
			   (random-camo num-col size file))))

(define do-custom-camo (lambda ()
			 (let ((colors '())
			       (size 0)
			       (file ""))
			   (display "enter filename: ")
			   (set! file (symbol->string (read))) 
			   (display "enter size (wide or high) in pixels: ")
			   (set! size (read))
			   (display "enter your colors") (newline)
			   (set! colors (append colors (get-all-colors colors)))
			   (make-camo colors size file))))
			 

(display "enter 1 for random camo, 2 for custom: ")
(define choice 0)
(set! choice (read))
(case choice
  ((1) (do-random-camo))
  ((2) (do-custom-camo)))
